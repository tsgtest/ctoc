﻿Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word

Namespace LMP.Numbering.CustomTOC
    Public Interface iCTOC
        ReadOnly Property PleadingPaperType() As Integer
        ReadOnly Property BPFile() As String
        ReadOnly Property HeaderBookmark() As String
        ReadOnly Property FooterBookmark() As String
        ReadOnly Property FirmName() As String
        ReadOnly Property Office() As String
        ReadOnly Property TopMargin() As Single
        ReadOnly Property BottomMargin() As Single
        ReadOnly Property LeftMargin() As Single
        ReadOnly Property RightMargin() As Single
        ReadOnly Property HeaderDistance() As Single
        ReadOnly Property FooterDistance() As Single
        ReadOnly Property FooterTitle() As String
        WriteOnly Property CurWordApp() As Word.Application
        Function lBeforeDialogShow(oTOCDlg As Object) As Long
        Function lAfterDialogShow() As Long
        Function lBeforeTOCSectionInsert() As Long
        Function lAfterTOCSectionInsert(rngSection As Word.Range, Optional bColsReformatted As Boolean = False) As Long
        Function lAfterTOCFieldInsert(fldTOC As Word.Field) As Long
        Function lBeforeTOCRework(rngTOC As Word.Range) As Long
        Function lInTOCRework(rngPara As Word.Range, xScheme As String, xExclusions As String, bIncludeStyles As Boolean, bIncludeTCEntries As Boolean) As Long
        Function lAfterTOCRework() As Long
        Function lGetTOCDetail(docTOC As Word.Document) As Long
        Function bIsCenteredFooter() As Boolean
        Function rngGetFirmNameTarget(rngTarget As Word.Range, secTOC As Word.Section) As Word.Range
    End Interface
End Namespace