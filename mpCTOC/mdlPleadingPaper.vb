Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports LMP.Numbering.CustomTOC.mdlVariables
Imports LMP.Forte.MSWord.Application

Namespace LMP.Numbering.CustomTOC
    Friend Class mdlPleadingPaper
        Public Shared Function bDetail(iPleadingPaperType As Integer, _
                 Optional ByRef xBPFile As String = "", _
                 Optional ByRef xBPRange As String = "", _
                 Optional ByRef xBPFooter As String = "", _
                 Optional ByRef sTopMargin As Single = 0, _
                 Optional ByRef sBottomMargin As Single = 0, _
                 Optional ByRef sLeftMargin As Single = 0, _
                 Optional ByRef sRightMargin As Single = 0, _
                 Optional ByRef sHeaderDistance As Single = 0, _
                 Optional ByRef sFooterDistance As Single = 0) As Boolean

            'fills args with appropriate values
            'for type iPleadingPaperType -
            '**************************************************
            '   THIS FUNCTION SHOULD REALLY REFERENCE THE
            '   PUBLIC DB TO GET THESE VALUES - IN THE FUTURE
            '   SOMEONE SHOULD CODE THIS AS SUCH
            '**************************************************
            Select Case iPleadingPaperType
                Case mpPleadingPaperTypes.mpPleadingPaperNone
                    xBPRange = "zzmpTOCHeader_Primary"
                    sTopMargin = mpTopMarginTOC
                    sBottomMargin = mpBottomMarginTOC
                    sLeftMargin = mpLeftMarginTOC
                    sRightMargin = mpRightMarginTOC
                    sHeaderDistance = mpHeaderDistTOC
                    sFooterDistance = mpFooterDistTOC

                Case mpPleadingPaperTypes.mpPleadingPaper28Line
                    xBPRange = "zzmpPleadingLineNo_TOHeader"
                    sTopMargin = mpPleadingTOCTopMargin
                    sBottomMargin = 0.5
                    sLeftMargin = mpPleading28LeftMargin
                    sRightMargin = mpPleading28RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance

                Case mpPleadingPaperTypes.mpPleadingPaper28LineFNLand
                    xBPRange = "zzmpPleadingLineNo_TOHeaderFN"
                    sTopMargin = mpPleadingTOCTopMargin
                    sBottomMargin = 0.5
                    sLeftMargin = mpPleading28LeftMargin
                    sRightMargin = mpPleading28RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance

                Case mpPleadingPaperTypes.mpPleadingPaper28LineFNPort
                    xBPRange = "zzmpPleadingLineNo_TOHeader"
                    sTopMargin = mpPleadingTOCTopMargin
                    sBottomMargin = 0.5
                    sLeftMargin = mpPleading28LeftMargin
                    sRightMargin = mpPleading28RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance

                Case mpPleadingPaperTypes.mpPleadingPaper26Line
                    xBPRange = "zzmpPleadingLineNo26_TOHeader"
                    sTopMargin = mpPleading26TOCTopMargin
                    sBottomMargin = mpPleading26BotMargin
                    sLeftMargin = mpPleading26LeftMargin
                    sRightMargin = mpPleading26RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance

                Case mpPleadingPaperTypes.mpPleadingPaper26LineFNLand
                    xBPRange = "zzmpPleadingLineNo26_TOHeaderFN"
                    sTopMargin = mpPleading26TOCTopMargin
                    sBottomMargin = mpPleading26BotMargin
                    sLeftMargin = mpPleading26LeftMargin
                    sRightMargin = mpPleading26RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance

                Case mpPleadingPaperTypes.mpPleadingPaper26LineFNPort
                    xBPRange = "zzmpPleadingLineNo26_TOHeader"
                    sTopMargin = mpPleading26TOCTopMargin
                    sBottomMargin = mpPleading26BotMargin
                    sLeftMargin = mpPleading26LeftMargin
                    sRightMargin = mpPleading26RightMargin
                    sHeaderDistance = mpPleadingHeaderDistance
                    sFooterDistance = mpPleadingFooterDistance
            End Select

            '---add pleading footer table if pleading document
            If g_bIsForte Then
                If bFooterHasFortePleadingPaper() Then
                    xBPFooter = "zzmpNothing"
                Else
                    xBPFooter = "zzmpTOCFooter"
                End If
            ElseIf Not g_oMP9PleadingPaper Is Nothing Then
                '9.9.5005 - no reason to insert boilerplate footers if they're
                'going to get wiped out by mp2k pleading paper
                xBPFooter = "zzmpNothing"
            ElseIf bIsPleadingDocument(g_oWordApp.ActiveDocument) Then
                If bIsSpecifiedTemplate(g_oWordApp.ActiveDocument, "Pleading Appellate") Then
                    xBPFooter = "zzmpPleadingFooter_AppTOC"
                ElseIf bIsSpecifiedTemplate(g_oWordApp.ActiveDocument, "Pleading CA") And _
                        bIsStateCourt() Then
                    xBPFooter = "zzmpPleadingFooterTOC"
                Else
                    xBPFooter = "zzmpPleadingFooterTOC_US"
                End If
            Else
                xBPFooter = "zzmpTOCFooter"
            End If
            bDetail = True
        End Function

        Public Shared Function iPleadingPaperType(rngHeader As Word.Range) As Integer
            With rngHeader.Find
                .Text = "26" & Chr(11) & "27" & Chr(11) & "28"
                .Execute()
                If .Found Then
                    iPleadingPaperType = 28
                Else
                    .Text = "24" & Chr(11) & "25" & Chr(11) & "26"
                    .Execute()
                    If .Found Then
                        iPleadingPaperType = 26
                    Else
                        iPleadingPaperType = 0
                    End If
                End If
            End With
        End Function
        Public Shared Function rngSetFirmNameTarget(rngTarget As Word.Range, _
                                    secSection As Word.Section) As Word.Range

            Dim iNumHeaderCols As Integer
            Dim iNumFooterCols As Integer


            '---determine if landscaped header col exists
            iNumHeaderCols = iFooterType(secSection _
                                .Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary))

            If iNumHeaderCols <> 3 Then    'firm name in footer
                '---        check the footer
                iNumFooterCols = iFooterType(secSection _
                                .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary))

                If iNumFooterCols = 0 Then     'no table, no firm name
                    rngSetFirmNameTarget = Nothing
                Else
                    rngSetFirmNameTarget = rngTarget.Tables(1) _
                                        .Cell(Row:=1, Column:=1).Range
                End If

            ElseIf iNumHeaderCols = 3 Then  'firm name in header
                rngSetFirmNameTarget = rngTarget.Tables(1) _
                                            .Cell(1, 1).Range
            End If

        End Function

        Public Shared Function iGetPleadingPaperType(docDocument As Word.Document) As Integer
            Dim xPPaper As String = ""
            Dim xPrefix As String = ""
            Dim xSuffix As String = ""
            Dim iLines As Integer
            Dim rngHeader As Word.Range
            Dim rngFooter As Word.Range
            Dim iNumHeaderCols As Integer
            Dim iNumFooterCols As Integer
            Dim rngFNTarget As Word.Range
            Dim secScope As Word.Section
            Dim iTestLen As Integer
            Dim dVar As Variable
            Dim bDVExists As Boolean

            iGetPleadingPaperType = 0
            '---First check for PP doc var
            For Each dVar In docDocument.Variables
                If dVar.Name = "zzmpFixed_PleadingPaperType" Then
                    bDVExists = True
                    Exit For
                End If
            Next
            If bDVExists Then
                xPPaper = docDocument.Variables("zzmpFixed_PleadingPaperType").Value
            Else
                '---See if there's pleading paper at all
                secScope = docDocument.Sections.First
                rngHeader = docDocument.Sections.First.Headers _
                                        (WdHeaderFooterIndex.wdHeaderFooterPrimary).Range
                rngFooter = docDocument.Sections.First.Footers _
                                        (WdHeaderFooterIndex.wdHeaderFooterPrimary).Range

                iLines = iPleadingPaperType(rngHeader)

                If iLines = 0 Then      'no pleading paper
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaperNone
                    Exit Function
                Else                    'determine paper type
                    Select Case iLines
                        Case 26
                            xPrefix = "PP26"

                        Case 28
                            xPrefix = "PP28"

                        Case Else
                    End Select
                    '---now check for a) three cols in header
                    iNumHeaderCols = iFooterType(secScope _
                                    .Headers(WdHeaderFooterIndex.wdHeaderFooterPrimary))

                    If iNumHeaderCols = 3 Then  'check for firm name text in first col

                        rngFNTarget = rngSetFirmNameTarget(rngHeader, _
                                                            secScope)
                        '---find out if there's any text
                        iTestLen = Len(rngFNTarget.Text)
                        If iTestLen < 5 Then
                            xSuffix = "None"
                        Else
                            xSuffix = "FNLandscaped"
                        End If

                    Else   'check for footer table

                        iNumFooterCols = iFooterType(secScope _
                                        .Footers(WdHeaderFooterIndex.wdHeaderFooterPrimary))
                        If iNumFooterCols <> 4 Then  'it's not a pleading footer table
                            xSuffix = "None"
                        Else
                            rngFNTarget = rngSetFirmNameTarget(rngFooter, _
                                                                secScope)
                            '---find out if there's any text
                            iTestLen = Len(rngFNTarget.Text)
                            If iTestLen < 5 Then
                                xSuffix = "None"
                            Else
                                xSuffix = "FNPortrait"
                            End If

                        End If

                    End If

                End If

                xPPaper = xPrefix & xSuffix

            End If

            '---now assign constant determined by PPtype string

            Select Case xPPaper
                Case "PP26None"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper26Line
                Case "PP26FNPortrait"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper26LineFNPort
                Case "PP26FNLandscaped"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper26LineFNLand
                Case "PP28None"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper28Line
                Case "PP28FNPortrait"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper28LineFNPort
                Case "PP28FNLandscaped"
                    iGetPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaper28LineFNLand

                Case Else
            End Select

        End Function

        Public Shared Function bIsStateCourt() As Boolean
            Dim xCourtVar As String
            On Error Resume Next
            xCourtVar = UCase(g_oWordApp.ActiveDocument.Variables("lstCourts").Value)
            Select Case xCourtVar
                Case "SUPERIOR", "MUNICIPAL", "OTHER", ""
                    bIsStateCourt = True
                Case Else
                    bIsStateCourt = False
            End Select

        End Function

        Public Shared Function bIsPleadingDocument(docDoc As Word.Document) As Boolean
            With docDoc
                If InStr(UCase(.AttachedTemplate.Name), "PLEAD") Or _
                        InStr(UCase(.AttachedTemplate.Name), "PROOF") Or _
                        InStr(UCase(.AttachedTemplate.Name), "VER") Then
                    bIsPleadingDocument = True
                Else
                    bIsPleadingDocument = False
                End If
            End With
        End Function

        Public Shared Function bIsSpecifiedTemplate(docDoc As Word.Document, _
                                      xTemplateName As String) As Boolean
            'returns true if name of attached template
            'of docDoc is xTemplateName
            Dim bTemp As Boolean

            bTemp = (InStr(UCase(docDoc.AttachedTemplate.Name), _
                           UCase(xTemplateName)) > 0)
            bIsSpecifiedTemplate = bTemp
        End Function

        Public Shared Function iFooterType(ftrFooter As HeaderFooter) As Integer
            'returns 0 if not a MacPac
            'footer (zzmpFooter style) -
            'else, returns an integer
            'representing # of columns

            Dim tFtrTable As Word.Table
            Dim xStylePrefix As String
            On Error Resume Next
            '   cycle through all tables in footer
            For Each tFtrTable In ftrFooter.Range.Tables
                With tFtrTable.Range
                    xStylePrefix = Left(.Style, 10)
                    If InStr(xStylePrefix, "Footer") Then
                        iFooterType = .Columns.Count
                        Exit Function
                    Else
                        iFooterType = .Columns.Count
                    End If
                End With
            Next tFtrTable
        End Function

        Private Shared Function bFooterHasFortePleadingPaper() As Boolean
            Dim oSection As Word.Section
            Dim oFooter As Word.HeaderFooter
            Dim oTag As Word.XMLNode
            Dim xObjectData As String
            Dim oCC As Word.ContentControl
            Dim xTag As String = ""
            Dim oBookmark As Word.Bookmark
            Dim xBmk As String = ""

            For Each oSection In g_oWordApp.ActiveDocument.Sections
                For Each oFooter In oSection.Footers
                    '1-15-14 - support for bookmark mSEGs
                    For Each oBookmark In oFooter.Range.Bookmarks
                        xBmk = oBookmark.Name
                        If UCase(Left$(xBmk, 4)) = "_MPS" Then
                            xObjectData = GetForteAttributeValue(Mid$(xBmk, 2), _
                                "ObjectData", g_oWordApp.ActiveDocument)
                            If InStr(xObjectData, "|ObjectTypeID=503|") > 0 Then
                                bFooterHasFortePleadingPaper = True
                                Exit Function
                            End If
                        End If
                    Next oBookmark

                    For Each oTag In oFooter.Range.XMLNodes
                        If oTag.BaseName = "mSEG" Then
                            xObjectData = oTag.SelectSingleNode("@ObjectData").NodeValue
                            xObjectData = Decrypt(xObjectData)
                            If InStr(xObjectData, "|ObjectTypeID=503|") > 0 Then
                                bFooterHasFortePleadingPaper = True
                                Exit Function
                            End If
                        End If
                    Next oTag

                    'content controls - added 8/11/10
                    For Each oCC In oFooter.Range.ContentControls
                        xTag = oCC.Tag
                        If Left$(xTag, 3) = "mps" Then
                            xObjectData = GetForteAttributeValue(xTag, "ObjectData", g_oWordApp.ActiveDocument)
                            If InStr(xObjectData, "|ObjectTypeID=503|") > 0 Then
                                bFooterHasFortePleadingPaper = True
                                Exit Function
                            End If
                        End If
                    Next oCC
                Next oFooter
            Next oSection

            bFooterHasFortePleadingPaper = False
        End Function

        Public Shared Function GetForteAttributeValue(ByVal xTag As String, ByVal xName As String, _
            Optional ByVal oDoc As Word.Document = Nothing) As String
            Dim xVar As String = ""
            Dim xValue As String = ""
            Dim lPos As Integer
            Dim lPos2 As Integer
            Dim iIndex As Integer
            Dim xDeletedScopes As String = ""
            Dim xTagID As String = ""
            Dim xAttributes As String = ""

            GetForteAttributeValue = ""

            If oDoc Is Nothing Then _
                oDoc = g_oWordApp.ActiveDocument

            If xName <> "DeletedScopes" Then
                'attribute is in mpo-prefixed variable
                xVar = "mpo" & Mid$(xTag, 4, 8)

                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo 0

                If xValue = "" Then _
                    Exit Function

                lPos = InStr(xValue, "��")
                xTagID = Left$(xValue, lPos - 1)
                xAttributes = Mid$(xValue, lPos + 2)
                If xName = "TagID" Or xName = "Name" Then
                    'these attributes are always at start of string,
                    'unlabeled and unencrypted
                    GetForteAttributeValue = xTagID
                Else
                    'search for specified attribute in encrypted portion of value
                    xAttributes = "��" & Decrypt(xAttributes) & "��"
                    lPos = InStr(xAttributes, "��" & xName & "=")
                    If lPos <> 0 Then
                        lPos = lPos + Len(xName) + 3
                        lPos2 = InStr(lPos, xAttributes, "��")
                        GetForteAttributeValue = Mid$(xAttributes, lPos, lPos2 - lPos)
                    End If
                End If
            Else
                'attribute is in mpd-prefixed variable
                iIndex = 1
                xVar = "mpd" & Mid$(xTag, 4, 8) & "01"

                On Error Resume Next
                xValue = oDoc.Variables(xVar).Value
                On Error GoTo 0

                'there can be multiple mpd doc vars
                While xValue <> ""
                    xDeletedScopes = xDeletedScopes & xValue
                    iIndex = iIndex + 1
                    xVar = Left$(xVar, 11) & New String("0", 2 - Len(CStr(iIndex))) & CStr(iIndex)
                    xValue = ""
                    On Error Resume Next
                    xValue = oDoc.Variables(xVar).Value
                    On Error GoTo 0
                End While

                GetForteAttributeValue = Decrypt(xDeletedScopes)
            End If
        End Function
    End Class
End Namespace

