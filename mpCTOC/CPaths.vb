﻿Option Explicit On

Imports LMP.Numbering.CustomTOC.cRegistry
Imports System.IO.Path
Imports System.Reflection.Assembly

Namespace LMP.Numbering.CustomTOC
    Friend Class cPaths
        Private Shared m_xAppPath As String

        Private Const CSIDL_LOCAL_APPDATA = &H1C&
        Private Const CSIDL_FLAG_CREATE = &H8000&
        Private Const CSIDL_COMMON_DOCUMENTS = &H2E
        Private Const SHGFP_TYPE_CURRENT = 0
        Private Const SHGFP_TYPE_DEFAULT = 1
        Private Const MAX_PATH = 260

        Declare Function GetKeyState Lib "user32" (ByVal nVirtKey As Integer) As Integer
        Declare Function SHGetFolderPath Lib "shfolder" _
            Alias "SHGetFolderPathA" _
            (ByVal hwndOwner As Long, ByVal nFolder As Long, _
            ByVal hToken As Long, ByVal dwFlags As Long, _
            ByVal pszPath As String) As Long

        Public Shared Function GetAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            Dim oReg As cRegistry
            Dim xPath As String = ""
            Dim lRet As Long
            Dim xTemp As String = ""

            If m_xAppPath = "" Then
                oReg = New cRegistry

                'look for new registry key first
                xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\Numbering", "DataDirectory")
                xPath = RTrim(xPath)

                If Len(xPath) = 0 Then
                    'look for old registry key value
                    xPath = oReg.GetLocalMachineValue("Software\Legal MacPac\9.x", "NumberingData")
                    xPath = RTrim(xPath)
                End If

                'exists, so set path to value
                If Len(xPath) > 0 Then
                    'substitute real path for user var if it exists in string
                    If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                        'use API to get Windows user name
                        xTemp = GetUserVarPath(CStr(xPath))
                    ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                        xTemp = GetCommonDocumentsPath(CStr(xPath))
                    Else
                        'use environmental variable
                        xTemp = GetEnvironVarPath(CStr(xPath))
                    End If

                    'trim trailing slash
                    If Right$(xTemp, 1) = "\" Then _
                        xTemp = Left$(xTemp, Len(xTemp) - 1)

                    'if ini is not in specified path, use App.Path
                    If Dir(xTemp & "\tsgNumbering.ini") = "" Then
                        xTemp = GetDirectoryName(GetExecutingAssembly().GetName().CodeBase)
                    End If
                Else
                    'registry value empty, so set path to App.Path
                    xTemp = GetDirectoryName(GetExecutingAssembly().GetName().CodeBase)
                End If

                xTemp = xTemp.Replace("file:\", "")
                m_xAppPath = xTemp
            End If

            GetAppPath = m_xAppPath
        End Function

        Public Shared Function GetCommonDocumentsPath(ByVal xPath As String) As String
            'substitutes user path for <UserName>
            Dim xBuf As String = ""

            '   get common documents folder
            xBuf = New String(" ", 255)

            SHGetFolderPath(0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf)

            If Len(xBuf) = 0 Then
                'alert to no logon name
                Throw New Exception("Common_Documents is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   trim extraneous buffer chars
            xBuf = RTrim(xBuf)
            xBuf = Left(xBuf, Len(xBuf) - 1)

            '   substitute common documents at specified point in path
            GetCommonDocumentsPath = xSubstitute(xPath, "<Common_Documents>", xBuf)
        End Function

        Public Shared Function GetUserVarPath(ByVal xPath As String) As String
            'substitutes user path for <xToken>
            Dim xBuf As String = ""
            Dim xToken As String = ""

            '   get token - we now accept either "USER" or "USERNAME"
            If InStr(UCase(xPath), "<USER>") > 0 Then
                xToken = "<User>"
            Else
                xToken = "<UserName>"
            End If

            '   get logon name
            xBuf = Environment.UserName()

            If Len(xBuf) = 0 Then
                '       alert to no logon name
                Throw New Exception("UserName is empty.  You might not be logged on to the system. " & _
                        "Please log off, and log on again.")
            End If

            '   substitute user name at specified point in path
            GetUserVarPath = xSubstitute(xPath, xToken, xBuf)
        End Function

        Public Shared Function GetEnvironVarPath(ByVal xPath As String) As String
            'substitutes environment variable for <xToken>;
            'if variable doesn't exist, returns path unchanged
            Dim xToken As String = ""
            Dim iPosStart As Integer
            Dim iPosEnd As Integer
            Dim xValue As String = ""

            iPosStart = InStr(xPath, "<")
            iPosEnd = InStr(xPath, ">")

            If (iPosStart > 0) And (iPosEnd > 0) Then
                xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
                xValue = Environ(xToken)
            End If

            If xValue <> "" Then
                GetEnvironVarPath = xSubstitute(xPath, "<" & xToken & ">", xValue)
            Else
                GetEnvironVarPath = xPath
            End If
        End Function

        Public Shared Function GetMacPacAppPath() As String
            'Substitute for App.Path call: first looks in the registry for Public documents location
            'If not found, then uses regular App.Path command
            Dim oReg As cRegistry
            Dim xPath As String = ""
            Dim lRet As Long
            Dim xTemp As String = ""

            oReg = New cRegistry

            'GLOG 5056 (2/1/12) - look for new registry key first
            xPath = oReg.GetLocalMachineValue("Software\The Sackett Group\MacPac 9.0", "DataDirectory")
            xPath = RTrim(xPath)

            If Len(xPath) = 0 Then
                'look for old registry key value
                xPath = oReg.GetLocalMachineValue("Software\Legal MacPac\9.x", "MacPacData")
                xPath = RTrim(xPath)
            End If

            'exists, so set path to value
            If Len(xPath) > 0 Then
                'substitute real path for user var if it exists in string
                If (InStr(UCase(xPath), "<USERNAME>") > 0) Then
                    'use API to get Windows user name
                    xTemp = GetUserVarPath(CStr(xPath))
                ElseIf (InStr(UCase(xPath), "<COMMON_DOCUMENTS>") > 0) Then
                    xTemp = GetCommonDocumentsPath(CStr(xPath))
                Else
                    'use environmental variable
                    xTemp = GetEnvironVarPath(CStr(xPath))
                End If

                If Dir(xTemp & "\MacPac.ini") = "" Then
                    'ini is not in specified location, so set path to App.Path
                    xTemp = oReg.GetComponentPath("MPO.CApplication")
                End If
            Else
                'registry value empty, so set path to App.Path
                xTemp = oReg.GetComponentPath("MPO.CApplication")
            End If

            'GLOG 5056 - we assume a trailing backslash
            If xTemp <> "" Then
                If Right$(xTemp, 1) <> "\" Then _
                    xTemp = xTemp & "\"
            End If

            GetMacPacAppPath = xTemp
        End Function

        Private Shared Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
            'replaces xSearch in
            'xString with xReplace -
            'returns modified xString -
            'NOTE: SEARCH IS NOT CASE SENSITIVE

            Dim iSeachPos As Integer
            Dim xNewString As String = ""

            '   get first char pos
            iSeachPos = InStr(UCase(xString), _
                              UCase(xSearch))

            '   remove switch all chars
            While iSeachPos
                xNewString = xNewString & _
                    Left(xString, iSeachPos - 1) & _
                    xReplace
                xString = Mid(xString, iSeachPos + Len(xSearch))
                iSeachPos = InStr(UCase(xString), _
                                  UCase(xSearch))

            End While

            xNewString = xNewString & xString
            xSubstitute = xNewString
        End Function
    End Class
End Namespace