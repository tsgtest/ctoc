Option Explicit On

Imports [Word] = Microsoft.Office.Interop.Word
Imports Microsoft.Office.Interop.Word
Imports Microsoft.Office.Core
Imports LMP.Numbering.CustomTOC.mdlVariables
Imports LMP.Numbering.CustomTOC.cPaths
Imports LMP.Numbering.CustomTOC.mdlEncrypt
Imports LMP.Numbering.CustomTOC.mdlPleadingPaper
Imports LMP.Forte.MSWord.Application

Namespace LMP.Numbering.CustomTOC
    Public Class cCustomTOC : Implements LMP.Numbering.CustomTOC.iCTOC

        '***************************************
        'Custom TOC Class
        'write any code that customizes the TOC
        'here - the names of the methods below
        'indicate the point at which the method
        'is invoked by MPTOC90!mpTOC.bInsertTOC -
        'PLEASE let me know if this is insufficient
        'for TOC customization - DF
        '***************************************

        'use this object to retrieve any dlg
        'settings - WARNING: you must be very
        'careful when using this object, as it
        'is late bound.
        Private m_oTOCDlg As Object
        Private m_xBP As String = ""
        Private m_xBP2 As String = ""
        Private m_xHeaderBookmark As String = ""
        Private m_xFooterBookmark As String = ""
        Private m_xFirmName As String = ""
        Private m_xOffice As String = ""
        Private m_sTopMargin As Single = 0
        Private m_sBottomMargin As Single = 0
        Private m_sLeftMargin As Single = 0
        Private m_sRightMargin As Single = 0
        Private m_sHeaderDistance As Single = 0
        Private m_sFooterDistance As Single = 0
        Private m_bLinkFooters As Boolean = False
        Private m_xFooterTitle As String = ""
        Private m_iPleadingPaperType As Integer = 0
        Private m_xUserIni As String = ""
        Private m_xAppIni As String = ""
        Private m_bTOCSectionInserted As Boolean = False
        Public Sub New()
            'Constructor code moved to separate Initialize Sub, since g_oWordApp needs to be set before it can run
        End Sub
        Private Sub Initialize()
            Dim xUserPath As String
            Dim xBP As String
            Dim xAppPath As String
            Dim oForte As Word.AddIn
            Dim oAddIn As COMAddIn

            'this to avoid running lAfterTOCSectionInsert when section was already in place
            m_bTOCSectionInserted = Not bTOCExists(g_oWordApp.ActiveDocument)

            'check for Forte - ensure presence of both Word and COM add-ins
            On Error Resume Next
            oForte = g_oWordApp.AddIns("Forte.dotm")
            If Not oForte Is Nothing Then
                oAddIn = g_oWordApp.COMAddIns.Item("ForteAddIn")
                If Not oAddIn Is Nothing Then
                    g_bIsForte = oAddIn.Connect
                End If
            Else
                oForte = g_oWordApp.AddIns("mp10.dotm")
                If Not oForte Is Nothing Then
                    oAddIn = g_oWordApp.COMAddIns.Item("MacPac102007")
                    If Not oAddIn Is Nothing Then
                        g_bIsForte = oAddIn.Connect
                    End If
                End If
            End If
            On Error GoTo 0

            'check for Mp2k
            If Not g_bIsForte Then
                g_xMP90Dir = GetMacPacAppPath()
                If g_xMP90Dir <> "" Then
                    g_iSeed = iGetSeed(g_oWordApp.System.PrivateProfileString(g_xMP90Dir & _
                        "MacPac.ini", "General", "Seed"))
                End If

                '9.9.5005 - check for mp2k pleading paper at the outset -
                'this will avoid unnecessary footer processing if it exists
                If m_bTOCSectionInserted And (g_xMP90Dir <> "") Then
                    On Error Resume Next
                    g_oMP9PleadingPaper = CreateObject("MPO.CPleadingPaper")
                    On Error GoTo 0
                    If Not g_oMP9PleadingPaper Is Nothing Then
                        If Not g_oMP9PleadingPaper.Exists Then
                            g_oMP9PleadingPaper = Nothing
                        End If
                    End If
                End If
            End If

            xAppPath = GetAppPath()
            m_xAppIni = xAppPath & "\tsgNumbering.ini"
            xUserPath = g_oWordApp.System.PrivateProfileString(m_xAppIni, "General", "UserDir")
            If (InStr(UCase(xUserPath), "<USERNAME>") > 0) Or _
                    (InStr(UCase(xUserPath), "<USER>") > 0) Then
                '       use API to get Windows user name
                xUserPath = GetUserVarPath(xUserPath)
            Else
                '       use environmental variable
                xUserPath = GetEnvironVarPath(xUserPath)
            End If
            If Dir(xUserPath & "\tsgNumbers.sty") = "" Then
                '       use Word's default path
                xUserPath = g_oWordApp.Application.Options _
                    .DefaultFilePath(WdDefaultFilePath.wdUserTemplatesPath)
            ElseIf Right(xUserPath, 1) = "\" Then
                xUserPath = Left(xUserPath, Len(xUserPath) - 1)
            End If
            m_xUserIni = xUserPath & "\NumTOC.ini"

            'GLOG 5299 - added ini key to disable boilerplate pleading paper
            If UCase(g_oWordApp.System.PrivateProfileString(m_xAppIni, "TOC", _
                    "DisableBoilerplatePleadingPaper")) = "TRUE" Then
                m_iPleadingPaperType = mpPleadingPaperTypes.mpPleadingPaperNone
            Else
                m_iPleadingPaperType = iGetPleadingPaperType(g_oWordApp.ActiveDocument)
            End If

            mdlPleadingPaper.bDetail(m_iPleadingPaperType, _
                m_xBP, m_xHeaderBookmark, m_xFooterBookmark, m_sTopMargin, _
                m_sBottomMargin, m_sLeftMargin, m_sRightMargin, m_sHeaderDistance, _
                m_sFooterDistance)

            '   get main boilerplate file
            xBP = g_oWordApp.System.PrivateProfileString(m_xAppIni, "TOC", "BoilerplateName1")
            If xBP <> "" Then
                xBP = "\" & xBP
            Else
                xBP = "\tsgNumberingBoilerpl.ate"
            End If
            If Dir(xAppPath & xBP) <> "" Then
                m_xBP = xAppPath & xBP
            Else
                m_xBP = xAppPath & "\bp.doc"
            End If

            '   see if firm is using multiple boilerplate files
            m_xBP2 = g_oWordApp.System.PrivateProfileString(m_xAppIni, "TOC", "BoilerplateName2")

            m_xFirmName = g_oWordApp.System.PrivateProfileString(m_xUserIni, "Firm", "Name")
            m_xOffice = g_oWordApp.System.PrivateProfileString(m_xUserIni, "Firm", "Office")
        End Sub

        Private Function bTOCExists(docCurrent As Word.Document) As Boolean
            'returns true if there's an existing TOC

            Const mpTOCPlaceholder As String = "TABLE OF CONTENTS PLACEHOLDER"
            Dim rngContent As Word.Range
            With docCurrent
                rngContent = docCurrent.Content
                bTOCExists = (.Bookmarks.Exists("mpTableOfContents") Or _
                    .Bookmarks.Exists("TableOfContents") Or _
                    .Bookmarks.Exists("PTOC") Or _
                    .Bookmarks.Exists("TOC") Or _
                    rngContent.Find.Execute(mpTOCPlaceholder) Or _
                    .TablesOfContents.Count)
            End With
        End Function

        Public Function bIsCenteredFooter() As Boolean Implements iCTOC.bIsCenteredFooter
            ' True if using a three column footer with 1st and 3rd columns equal width
            If m_xFooterBookmark = "zzmpTOCFooter" Or _
                    m_xFooterBookmark = "zzmpPleadingFooter_AppTOC" Then
                bIsCenteredFooter = True
            Else
                bIsCenteredFooter = False
            End If
        End Function

        Public ReadOnly Property BottomMargin As Single Implements iCTOC.BottomMargin
            Get
                BottomMargin = m_sBottomMargin
            End Get
        End Property

        Public ReadOnly Property BPFile As String Implements iCTOC.BPFile
            Get
                BPFile = m_xBP
            End Get
        End Property

        Public ReadOnly Property FirmName As String Implements iCTOC.FirmName
            Get
                FirmName = m_xFirmName
            End Get
        End Property

        Public ReadOnly Property FooterBookmark As String Implements iCTOC.FooterBookmark
            Get
                FooterBookmark = m_xFooterBookmark
            End Get
        End Property

        Public ReadOnly Property FooterDistance As Single Implements iCTOC.FooterDistance
            Get
                FooterDistance = m_sFooterDistance
            End Get
        End Property

        Public ReadOnly Property FooterTitle As String Implements iCTOC.FooterTitle
            Get
                FooterTitle = m_xFooterTitle
            End Get
        End Property

        Public ReadOnly Property HeaderBookmark As String Implements iCTOC.HeaderBookmark
            Get
                HeaderBookmark = m_xHeaderBookmark
            End Get
        End Property

        Public ReadOnly Property HeaderDistance As Single Implements iCTOC.HeaderDistance
            Get
                HeaderDistance = m_sHeaderDistance
            End Get
        End Property

        Public ReadOnly Property Office As String Implements iCTOC.Office
            Get
                Office = m_xOffice
            End Get
        End Property

        Public ReadOnly Property PleadingPaperType As Integer Implements iCTOC.PleadingPaperType
            Get
                PleadingPaperType = m_iPleadingPaperType
            End Get
        End Property

        Public ReadOnly Property RightMargin As Single Implements iCTOC.RightMargin
            Get
                RightMargin = m_sRightMargin
            End Get
        End Property

        Public ReadOnly Property LeftMargin As Single Implements iCTOC.LeftMargin
            Get
                LeftMargin = m_sLeftMargin
            End Get
        End Property

        Public ReadOnly Property TopMargin As Single Implements iCTOC.TopMargin
            Get
                TopMargin = m_sTopMargin
            End Get
        End Property

        Public Function lAfterDialogShow() As Long Implements iCTOC.lAfterDialogShow
            Dim xBP As String
            Dim xAppPath As String

            '   custom code starts here
            '   if firm is using multiple boilerplate files, switch to one specified in TOC dialog
            If m_xBP2 <> "" Then
                xBP = g_oWordApp.System.PrivateProfileString(m_xUserIni, "TOC", "CurrentBoilerplate")
                xAppPath = GetAppPath()
                If (xBP <> "") And _
                        (Dir(xAppPath & "\" & xBP) <> "") Then
                    m_xBP = xAppPath & "\" & xBP
                End If
            End If

            lAfterDialogShow = 0
        End Function

        Public Function lAfterTOCFieldInsert(fldTOC As Field) As Long Implements iCTOC.lAfterTOCFieldInsert
            '   custom code starts here

            lAfterTOCFieldInsert = 0
        End Function

        Public Function lAfterTOCRework() As Long Implements iCTOC.lAfterTOCRework
            '   custom code starts here

            lAfterTOCRework = 0
        End Function

        Public Function lAfterTOCSectionInsert(rngSection As Range, Optional bColsReformatted As Boolean = False) As Long Implements iCTOC.lAfterTOCSectionInsert
            Dim oSection As Word.Section
            Dim rngLocation As Word.Range
            Dim oHeader As Word.HeaderFooter
            Dim bSecondTry As Boolean
            Dim oBookmark As Word.Bookmark
            Dim rngBookmark As Word.Range
            Dim lStart As Long

            '   custom code starts here
            oSection = rngSection.Sections(1)

            If m_bTOCSectionInserted Then
                If g_bIsForte Then
                    'MacPac 10.x
                    rngLocation = rngSection.Duplicate

                    'GLOGv2 7398 (8/27/14) - shrink any mSEG bookmarks that
                    'have expanded into the TOC section
                    If oSection.Index = 1 Then
                        lStart = g_oWordApp.ActiveDocument.Sections(2).Range.Start
                        For Each oBookmark In rngLocation.Bookmarks
                            If oBookmark.Name Like "_mps*" Then
                                If oBookmark.Range.End >= lStart Then
                                    rngBookmark = g_oWordApp.ActiveDocument.Range( _
                                        lStart, oBookmark.Range.End)
                                    rngBookmark.Bookmarks.Add(oBookmark.Name)
                                End If
                            End If
                        Next oBookmark
                    End If

                    rngLocation.StartOf()

                    'insert temporary bookmark to allow mp10 to identify this as TOC section
                    oBookmark = rngLocation.Bookmarks.Add("mpTableOfContents_Temp")

                    'insert pleading paper if necessary
                    rngLocation.Select()
                    On Error Resume Next
                    g_oWordApp.Run("zzmpInsertPleadingPaper")
                    On Error GoTo 0

                    'delete temporary bookmark
                    oBookmark.Delete()

                    '10/31/11 - delete temporary TOA bookmark if it exists
                    If rngSection.Document.Bookmarks.Exists("zzmpTEMP_TOA") Then _
                        rngSection.Document.Bookmarks("zzmpTEMP_TOA").Delete()
                ElseIf Not g_oMP9PleadingPaper Is Nothing Then
                    With g_oMP9PleadingPaper
                        'if .Exists Then
                        '---9.4.0 refresh all the properties.
                        .PleadingPaperDef.ID = .PleadingPaperDef.ID
                        .StartPageNumberingAt = 1
                        .StartingPageNumber = 1
                        .SideBarType = .SideBarType
                        .FirmName = .FirmName
                        .OfficeLocation = .OfficeLocation
                        .CaseNumber = .CaseNumber
                        .CaseNumberSeparator = .CaseNumberSeparator
                        .DocTitle = .DocTitle
                        .State = .State
                        .CustomProperties.RefreshValues()
                        .Update(oSection.Index, True, True, False)
                    End With
                    g_oMP9PleadingPaper = Nothing
                End If
            End If

            '   reformat header to match columns
            If bColsReformatted Then
                For Each oHeader In oSection.Headers
                    bSecondTry = False
                    rngLocation = oHeader.Range
                    With rngLocation
labSearch:
                        With .Find
                            .ClearFormatting()
                            .Forward = True
                            .Text = "Page"
                            .Execute()
                            If .Found Then
                                With .Parent
                                    .Expand(WdUnits.wdParagraph)
                                    '                           don't mistake page # in header for page label
                                    If .Fields.Count = 0 Then
                                        '                               if multi-column, hide label
                                        .Font.Hidden = (oSection.PageSetup _
                                            .TextColumns.Count > 1)
                                    Else
                                        If Not bSecondTry Then
                                            '                                   look for another instance
                                            bSecondTry = True
                                            .EndOf()
                                            GoTo labSearch
                                        End If
                                    End If
                                End With
                            End If
                        End With
                    End With
                Next oHeader
            End If

            lAfterTOCSectionInsert = 0
        End Function

        Public Function lBeforeDialogShow(oTOCDlg As Object) As Long Implements iCTOC.lBeforeDialogShow
            Dim i As Integer
            Dim oRange As Word.Range
            Dim xTag As String
            Dim xObjectData As String
            Dim oTag As Word.XMLNode
            Dim oObjectData As Word.XMLNode
            Dim lShowTags As Long
            Dim j As Integer
            Dim oDoc As Word.Document

            oDoc = g_oWordApp.ActiveDocument

            If g_bIsForte Then
                'GLOG 8866 (dm) - with the switch to bookmark mSEGS, we're no longer discovering unlinked
                'or empty Forte TOA
                If (oDoc.TablesOfAuthorities.Count = 0) And Not oDoc.Bookmarks.Exists("zzmpTEMP_TOA") Then
                    '10/31/11 - search for mp10 TOA -
                    'if found, add temporary bookmark at start of section
                    For i = 1 To oDoc.Sections.Count
                        oRange = oDoc.Sections(i).Range

                        'GLOG 8866 (dm) - check for mSEG bookmark first - looking at first two bookmarks
                        'in section in case mSEG and mBlock sequence is reversed
                        For j = 1 To 2
                            If oRange.Bookmarks.Count >= j Then
                                xTag = oRange.Bookmarks(j).Name
                                If Left$(UCase(xTag), 4) = "_MPS" Then
                                    xTag = Mid$(xTag, 2)
                                    xObjectData = GetForteAttributeValue(xTag, "ObjectData", oDoc)
                                    If InStr(xObjectData, "|ObjectTypeID=516|") > 0 Then
                                        oRange.StartOf()
                                        oRange.Bookmarks.Add("zzmpTEMP_TOA")
                                        Exit For
                                    End If
                                End If
                            End If
                        Next j

                        If oRange.ContentControls.Count > 0 Then
                            xTag = oRange.ContentControls(1).Tag
                            If Left$(xTag, 3) = "mps" Then
                                xObjectData = GetForteAttributeValue(xTag, "ObjectData", oDoc)
                                If InStr(xObjectData, "|ObjectTypeID=516|") > 0 Then
                                    oRange.StartOf()
                                    oRange.Bookmarks.Add("zzmpTEMP_TOA")
                                    Exit For
                                End If
                            End If
                        ElseIf oRange.XMLNodes.Count > 0 Then
                            oTag = oRange.XMLNodes(1)
                            If oTag.BaseName = "mSEG" Then
                                oObjectData = oTag.SelectSingleNode("@ObjectData")
                                If Not oObjectData Is Nothing Then
                                    xObjectData = Decrypt(oObjectData.NodeValue)
                                    If InStr(xObjectData, "|ObjectTypeID=516|") > 0 Then
                                        lShowTags = g_oWordApp.ActiveWindow.View.ShowXMLMarkup
                                        g_oWordApp.ActiveWindow.View.ShowXMLMarkup = 1
                                        oRange.StartOf()
                                        oRange.Bookmarks.Add("zzmpTEMP_TOA")
                                        g_oWordApp.ActiveWindow.View.ShowXMLMarkup = lShowTags
                                        Exit For
                                    End If
                                End If
                            End If
                        End If
                    Next i
                End If
            End If

            lBeforeDialogShow = 0
        End Function

        Public Function lBeforeTOCRework(rngTOC As Range) As Long Implements iCTOC.lBeforeTOCRework
            'runs before the paragraphs of the
            'toc are modified by MacPac code
            '   custom code starts here

            lBeforeTOCRework = 0
        End Function

        Public Function lBeforeTOCSectionInsert() As Long Implements iCTOC.lBeforeTOCSectionInsert
            '   custom code starts here

            lBeforeTOCSectionInsert = 0
        End Function

        Public Function lGetTOCDetail(docTOC As Document) As Long Implements iCTOC.lGetTOCDetail
            '   custom code starts here

            lGetTOCDetail = 0
        End Function

        Public Function lInTOCRework(rngPara As Range, xScheme As String, xExclusions As String, bIncludeStyles As Boolean, bIncludeTCEntries As Boolean) As Long Implements iCTOC.lInTOCRework
            'this function is called X times
            'by MPTOC9!mpTOC.bReworkTOC - ie
            'it is called once per toc para -
            'rngPara points to the current toc
            'paragraph - if necessary, expand
            'rngPara to whole paragraph (left out
            'for speed reasons)
            '   custom code starts here

            lInTOCRework = 0
        End Function

        Public Function rngGetFirmNameTarget(rngTarget As Range, secTOC As Section) As Range Implements iCTOC.rngGetFirmNameTarget
            rngGetFirmNameTarget = mdlPleadingPaper.rngSetFirmNameTarget(rngTarget, secTOC)
        End Function

        Public WriteOnly Property CurWordApp As Word.Application Implements iCTOC.CurWordApp
            Set(value As Word.Application)
                g_oWordApp = value
                Initialize()
            End Set
        End Property
    End Class
End Namespace


